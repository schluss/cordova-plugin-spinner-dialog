package hu.dpal.phonegap.plugins;

import java.util.Stack;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.widget.ProgressBar;
import android.graphics.drawable.ColorDrawable;
import android.graphics.Color;
import android.R;

public class SpinnerDialog extends CordovaPlugin {

	public Stack<ProgressDialog> spinnerDialogStack = new Stack<ProgressDialog>();

	public SpinnerDialog() {
	}

	public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
		if (action.equals("show")) {

			final String title = "null".equals(args.getString(0)) ? null : args.getString(0);
			final String message = "null".equals(args.getString(1)) ? null : args.getString(1);
			final boolean isFixed = args.getBoolean(2);
                
			final CordovaInterface cordova = this.cordova;
			Runnable runnable = new Runnable() {
				public void run() {
					
					DialogInterface.OnCancelListener onCancelListener = new DialogInterface.OnCancelListener() {
						public void onCancel(DialogInterface dialog) {
							if (!isFixed) {
								while (!SpinnerDialog.this.spinnerDialogStack.empty()) {
									SpinnerDialog.this.spinnerDialogStack.pop().dismiss();
									callbackContext.success();
								}
							}
						}
					};
					
					ProgressDialog dialog;
					if (isFixed) {
						dialog = CallbackProgressDialog.show(cordova.getActivity(), title, message, true, false, null, callbackContext);
					} else {
						int styleId = cordova.getActivity().getResources().getIdentifier("AlertDialogCustom", "style", cordova.getActivity().getPackageName());

						dialog = new ProgressDialog(cordova.getActivity(),styleId);
						dialog.setTitle(title);
						dialog.setMessage(message);
						dialog.setIndeterminate(true);
						dialog.setCancelable(true);
						dialog.setOnCancelListener(onCancelListener);
						dialog.show();
					}
					
					if (title == null && message == null) {
						ProgressBar progressBar = new ProgressBar(cordova.getActivity());
						progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#FA4A69"), android.graphics.PorterDuff.Mode.MULTIPLY);
						progressBar.setAlpha(0.7f);
						//progressBar.setScaleY(1f);

						dialog.setContentView(progressBar);
					}
					
					SpinnerDialog.this.spinnerDialogStack.push(dialog);

				}
			};
			this.cordova.getActivity().runOnUiThread(runnable);

		} else if (action.equals("hide")) {

			Runnable runnable = new Runnable() {
				public void run() {

					if (!SpinnerDialog.this.spinnerDialogStack.empty()) {
						SpinnerDialog.this.spinnerDialogStack.pop().dismiss();
					}

				}
			};
			this.cordova.getActivity().runOnUiThread(runnable);

		}
		return true;
	}

}
